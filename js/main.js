//-----------------------------------------------------------------------
//todo:
//-------------------------------------
//PERFORMANCE: 
//make animations smooth - try to run at 60 fps
//build using phonegap and test performance on phone - look at crosswalk too
//see if there is a way to select lower fps in canvas, it always seems to be aiming for 60
//see if frameskipping helps performance or not *****
//-------------------------------------
//PHONEFRIENDLY:
//add swiping instead/aswell of clicking ******
//think about layout
//-------------------------------------
//GAME SHIZ:
//sort out speed of arrows: use angles and speeds instead of gradients ****
//give arrows the ability to rotate
//think about paths which aren't straight
//-------------------------------------
//FINISHING TOUCHES
//consider changing scaling so that whole screen is used but coordinates are still scaled
//sort out HTML and CSS
//sort out score text
//look at drawing something interesting in the part of the canvas which isn't updated
//add splash screens ++++++
//add menu system ++++++
//add fart sounds +++++++
//add level system ******
//create levels ******
//-----------------------------------------------------------------------
var Game = function() {
    //fps vars
    this.filterStrength = 20;
    this.frameTime = 0;
    this.lastLoop = new Date();
    this.thisLoop = new Date();
    //fps vars
    this.canvas = $("#main-canvas")[0];
    this.context = this.canvas.getContext('2d');
    this.paused = true;
    this.interval = null;
    this.arrows = [];
    this.circle = null;
    this.addKeyListeners();
    this.addEventListeners();
    this.lastAnimationFrame = 0;
    this.lastArrowUpdate = 0;
    this.lastClick = null;
    this.thisClick = null;
    this.score = 0;
    this.scoreMultiplier = 10;
    this.scorePosition = [0.1, 0.1];
    this.DEBUG = true;
    this.numberOfArrows = 0;
    //Music
    this.musicElement = document.getElementById('fuck-music');
    this.musicCheckboxElement = document.getElementById('fuck-music-checkbox');
    this.musicElement.volume = 1.0;
    this.musicOn = this.musicCheckboxElement.checked;
    this.POLL_INTERVAL = 1000; //poll every second
    this.SOUNDTRACKLENGTH = 104; //seconds
    this.startMusic();
};

Game.prototype.fps = function() {
    $("title").text((1000/this.frameTime).toFixed(1) + " fps" );
}

Game.prototype.run = function() {
    //this.interval = window.setInterval(this.update.bind(this), 1000 / 60);
    //this.fpsInterval = window.setInterval(this.fps.bind(this), 1000);
    //this.pollMusicInterval = window.setInterval(this.pollMusic.bind(this), this.POLL_INTERVAL);
    window.requestAnimationFrame(this.update.bind(this));

    this.paused = false;

    circle = new fuck_circle(0.5, 0.5);
    this.circle = circle;
    //window.requestAnimationFrame(this.update.bind(this));
};

Game.prototype.pause = function() {
    if (this.interval != null) {
        window.clearInterval(this.interval);
        this.interval = null;
    }
    this.paused = true;
    this.musicElement.pause();
};

Game.prototype.update = function(now) {
    for(var i = 0; i < this.arrows.length; i++) {
        this.arrows[i].update(now - this.lastAnimationFrame); //arrows get told the delta
    }

    if(this.arrows.length !== this.numberOfArrows) {
        console.log("There are: ", this.arrows.length, " arrows in memory");
        this.numberOfArrows = this.arrows.length;
    }

    this.circle.update();

    //randomly generate new arrows here
    if( (Number( Date.now()) - this.lastArrowUpdate) > 3000 ) {
        this.lastArrowUpdate = Date.now();
        var orientations = ["down", "downleft", "left", "upleft", "up", "upright", "right", "downright"];
        //var orientations = ["down", "down", "down", "down", "down", "down", "down", "down"];
        var orientation = Math.floor(Math.random() * 8);
        var xord = Math.floor(Math.random() * (1.2 + 1)) - 0.1; //check/fix these
        var yord = Math.floor(Math.random() * (1.2 + 1)) - 0.1; //check/fix these
        var Arrow = new fuck_arrow(xord, yord, orientations[orientation]);
        this.arrows.push(Arrow);
    }

    for(var i = 0; i < this.arrows.length; i++) {
        if (this.arrows[i].deleteMe === true) {
            this.score -= this.scoreMultiplier;
            this.arrows.splice(i, 1);
        }
    }
    this.draw(now);
    this.fps();
    this.lastAnimationFrame = now;
    requestAnimationFrame(this.update.bind(this));
};

Game.prototype.draw = function(now) {
    // Set the canvas to the correct size if the window is resized
    if (this.canvas.width != this.canvas.offsetWidth ||
        this.canvas.height != this.canvas.offsetHeight) {
        this.canvas.width = this.canvas.offsetWidth;
        this.canvas.height = this.canvas.offsetHeight;
    }

    this.context.save();

    //fps code
    this.thisFrameTime = (this.thisLoop = new Date()) - this.lastLoop;
    this.frameTime += (this.thisFrameTime - this.frameTime) / this.filterStrength;
    this.lastLoop = this.thisLoop;
    //fps code

    //this block resizes the screen to give a square but i don't think i want that
    var minSize = Math.min(this.canvas.offsetWidth, this.canvas.offsetHeight);
    if (this.canvas.offsetWidth < this.canvas.offsetHeight) {
        this.diff = this.canvas.offsetHeight - this.canvas.offsetWidth;
        this.context.translate(0, this.diff / 2);
    }
    else {
        this.diff = this.canvas.offsetWidth - this.canvas.offsetHeight;
        this.context.translate(this.diff / 2, 0);
    }
    this.context.scale(minSize, minSize);
    this.context.clearRect(-1, -1, 3, 3); //clear the bits outside of our 0 to 1 range, too.

    //call the draw methods of the objects
    for (var i=0; i<this.arrows.length; i++) {
        var arrow = this.arrows[i];
        arrow.draw(this.context);
    }

    this.circle.draw(this.context);
    this.context.font = "italic bold 0.05pt Tahoma";
    this.context.fillText("Score" + String(this.score), this.scorePosition[0], this.scorePosition[1]); //FINISH ME

    //testing - this should be html
    //this.context.fillStyle="#FF0000";
    //this.context.fillRect(-1, -1, 1, 2);
    //imageObj = new Image();
    //imageObj.src = 'img/beardshack.jpg';
    //this.context.drawImage(imageObj, -0.5 - ((imageObj.width * 0.001) / 2), 0.5 - ((imageObj.height * 0.001) / 2), imageObj.width * 0.001, imageObj.height * 0.001);
    //this.context.fillRect(1, 0, 1, 1);
    //this.context.drawImage(imageObj, 1.5 - ((imageObj.width * 0.001) / 2), 0.5 - ((imageObj.height * 0.001) / 2), imageObj.width * 0.001, imageObj.height * 0.001);
    //end test

    this.context.restore();

    /*if (!this.paused) {
        window.requestAnimationFrame(this.draw.bind(this));
    }*/
};

Game.prototype.pollMusic = function() {
    if(this.musicElement.currentTime > this.SOUNDTRACKLENGTH) {
        this.restartMusic();
    }
};

Game.prototype.restartMusic = function() {
    this.musicElement.pause();
    this.musicElement.currentTime = 0;
    this.startMusic();
};

Game.prototype.startMusic = function() {
    if (this.musicCheckboxElement.checked) {
        this.musicElement.play();
    }
};

Game.prototype.addKeyListeners = function() {
    window.onkeydown = this.onKeyDown.bind(this);
    window.onkeyup = this.onKeyUp.bind(this);
    window.onmousedown = this.onMouseLeftDown.bind(this);
    window.onmouseup = this.onMouseLeftUp.bind(this);
};

Game.prototype.addEventListeners = function() {
    window.onchange = this.musicEventListener.bind(this);
};

Game.prototype.onKeyDown = function(event) {
    if (event.keyCode == 37) { // Left
    }
    else if (event.keyCode == 38) { // Up
    }
    else if (event.keyCode == 39) { // Right
    }
    else if (event.keyCode == 40) { // Down
    }
};

Game.prototype.onKeyUp = function(event) {
    if (event.keyCode == 37 || event.keyCode == 39) { // Left or Right
    }
    else if (event.keyCode == 38 || event.keyCode == 40) { // Up or Down
    }
};

Game.prototype.musicEventListener = function(event) {
    this.musicOn = this.musicCheckboxElement.checked;
    if (this.musicOn) {
        this.musicElement.play();
    }
    else {
        this.musicElement.pause();
    }
};

Game.prototype.onMouseLeftDown = function(event) {
    var mouseX, mouseY;

    if(event.offsetX) {
        mouseX = event.offsetX;
        mouseY = event.offsetY;
    }
    else if(event.layerX) {
        mouseX = event.layerX;
        mouseY = event.layerY;
    }
    
    if (this.canvas.offsetWidth < this.canvas.offsetHeight) {
        if( (mouseY >= (this.diff / 2)) && (mouseY <= (this.canvas.offsetHeight - (this.diff / 2))) )  {
            var scaledX = mouseX / this.canvas.offsetWidth;
            var scaledY = (mouseY - (this.diff / 2)) / (this.canvas.offsetHeight - this.diff);
            this.lastClick = [scaledX, scaledY];
        }
    }
    else {
        if ( (mouseX >= (this.diff / 2)) && (mouseX <= (this.canvas.offsetWidth - (this.diff / 2))) ) {
            var scaledX = (mouseX - (this.diff / 2)) / (this.canvas.offsetWidth - this.diff);
            var scaledY = mouseY / this.canvas.offsetHeight;
            this.lastClick = [scaledX, scaledY];
        }
    }
};

Game.prototype.onMouseLeftUp = function(event) {
    var mouseX, mouseY;

    if(event.offsetX) {
        mouseX = event.offsetX;
        mouseY = event.offsetY;
    }
    else if(event.layerX) {
        mouseX = event.layerX;
        mouseY = event.layerY;
    }
    
    if (this.canvas.offsetWidth < this.canvas.offsetHeight) {
        if( (mouseY >= (this.diff / 2)) && (mouseY <= (this.canvas.offsetHeight - (this.diff / 2))) )  {
            
            var scaledX = mouseX / this.canvas.offsetWidth;
            var scaledY = (mouseY - (this.diff / 2)) / (this.canvas.offsetHeight - this.diff);
            this.thisClick = [scaledX, scaledY];
        }
    }
    else {
        if ( (mouseX >= (this.diff / 2)) && (mouseX <= (this.canvas.offsetWidth - (this.diff / 2))) ) {
            
            var scaledX = (mouseX - (this.diff / 2)) / (this.canvas.offsetWidth - this.diff);
            var scaledY = mouseY / this.canvas.offsetHeight;
            this.thisClick = [scaledX, scaledY];
        }
    }
    //if you clicked in the right direction, remove the ARROW
    for (var i = 0; i < this.arrows.length; i++) {
        if ( vec2.distance(this.arrows[i].position, this.circle.position) < this.circle.radius ) {
            
            var clickAngle = (Math.atan2(this.lastClick[1] - this.thisClick[1], this.lastClick[0] - this.thisClick[0]) + (5 * Math.PI / 2)) % (2 * Math.PI);
            //clickAngle = (clickAngle + (2 * Math.PI)) % (2 * Math.PI); //to make negative residues positive.
            //console.log("clickAngle is: ", clickAngle);
            
            //atan2 calculates angles differently to context.rotate() so I add 360 and a quarter to put the starting point (or 0 radians) where I want and to
            //ensure that all values are positive. I think mod by 2pi to make sure I never get a value higher than 2pi.
            //below we give 0.2 radians of leeway either side, this is about 11.5 degrees. 0.2 was a guess.

            var upperLimit = (((this.arrows[i].orientationAngle + 0.2) % (2 * Math.PI)) + 2 * Math.PI) % (2 * Math.PI);
            var lowerLimit = (((this.arrows[i].orientationAngle - 0.2) % (2 * Math.PI)) + 2 * Math.PI) % (2 * Math.PI);
            //var upperLimit = this.arrows[i].orientationAngle + 0.2;
            //var lowerLimit = this.arrows[i].orientationAngle - 0.2;
            
            if (this.arrows[i].orientationAngle === 0) {
                if ( (clickAngle > lowerLimit) || (clickAngle < upperLimit) ) {
                    this.score += this.scoreMultiplier;
                    this.arrows[i].deleteMe = true;
                    this.arrows.splice(i, 1);
                    if (this.DEBUG) {
                        console.log("direction is correct and down");
                        console.log("you clicked: ", clickAngle);
                    }
                }
                else {
                    //this.score -= this.scoreMultiplier;
                    if (this.DEBUG) {
                        console.log("direction is wrong and arrow was down");
                        console.log("you clicked: ", clickAngle);
                        console.log("lower limit: ", lowerLimit);
                        console.log("upper limit: ", upperLimit);
                    }
                }
                //lel
            }
            else {
                if ( (clickAngle <= upperLimit) && (clickAngle >= lowerLimit) ) {
                    this.score += this.scoreMultiplier;
                    this.arrows[i].deleteMe = true;
                    this.arrows.splice(i, 1);
                    if (this.DEBUG) {
                        console.log("direction is correct");
                        console.log("you clicked: ", clickAngle);
                    }
                }
                else {
                    //this.score -= this.scoreMultiplier;
                    if (this.DEBUG) {
                        console.log("direction is wrong: ", clickAngle);
                        console.log("lowerLimit: ", lowerLimit, " upperLimit: ", upperLimit);
                    }
                }
            }
        }
        else {
            this.score -= this.scoreMultiplier;
            if (this.DEBUG) {
                console.log("no arrow in circle");
            }
        }
    }
};

$(document).ready(function() {
    window.game = new Game();
    window.game.run();
});