var fuck_arrow = function(x, y, orientation) {
    this.position = vec2.create();
    this.speed = 0.3;
    this.image = new Image();
    this.image.src = 'img/arrow.png';
    this.position[0] = x;
    this.position[1] = y;
    this.orientation = orientation;
    this.orientationAngle = null;
    this.offScreen = null;
    this.beenOnScreen = false;
    this.deleteMe = false;
    this.velocity = vec2.fromValues( 0.5 - this.position[0], 0.5 - this.position[1] );
    vec2.normalize(this.velocity, this.velocity);
    vec2.scale( this.velocity, this.velocity, this.speed );
    this.currentVelocity = vec2.clone(this.velocity);

    if(this.orientation === "down") {
        this.orientationAngle = 0;
        //the stock picture points down
    }
    else if (this.orientation === "downleft") {
        this.orientationAngle = Math.PI / 4;
    }
    else if (this.orientation === "left") {
        this.orientationAngle = Math.PI / 2;
    }
    else if (this.orientation === "upleft") {
        this.orientationAngle = 3 * Math.PI / 4;
    }
    else if (this.orientation === "up") {
        this.orientationAngle = Math.PI;
    }
    else if (this.orientation === "upright") {
        this.orientationAngle = 5 * Math.PI / 4;
    }
    else if (this.orientation === "right") {
        this.orientationAngle = 3 * Math.PI / 2;
    }
    else if (this.orientation === "downright") {
        this.orientationAngle = 7 * Math.PI / 4;
    }
    else {
        console.log("ERROR: orientation doesn't match any allowed orientation: ", this.orientation);
    }
};

fuck_arrow.prototype.update = function(now) {
    vec2.scale( this.currentVelocity, this.velocity, (now / 1000) );
    vec2.add(this.position, this.position, this.currentVelocity);
    
    if(this.position[0] > 0 && this.position[0] < 1 && this.position[1] > 0 && this.position[1] < 1) {
        this.beenOnScreen = true;
        this.offScreen = false;
    }
    if(this.position[0] < 0 || this.position[0] > 1 || this.position[1] < 0 || this.position[1] > 1) {
        if (this.beenOnScreen === true) {
            this.deleteMe = true;
            //delete me from memory, I've been on screen before and now I'm offscreen
        }
        this.offScreen = true;
    }
};

fuck_arrow.prototype.draw = function(context) {
    context.save();
    
    context.translate(this.position[0], this.position[1]);
    context.rotate(this.orientationAngle);
    context.drawImage(this.image, 0 - ((this.image.width * 0.001) / 2), 0 - ((this.image.height * 0.001) / 2), this.image.width * 0.001, this.image.height * 0.001);

    context.restore();
};